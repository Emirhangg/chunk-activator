# Chunk Activator

Keep chunks loaded even when you're away.

## Information

Check out this mod on [CurseForge][].

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/chunk-activator.git
cd chunk-activator
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```

[CurseForge]: https://minecraft.curseforge.com/projects/chunk-activator
